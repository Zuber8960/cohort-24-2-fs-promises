const path  = require("path");
const problem1 = require("../problem1.cjs");

const absolutePathOfRandomDirectory = path.join(__dirname, "../random");
const randomNumberOfFiles = 10;

problem1(absolutePathOfRandomDirectory, randomNumberOfFiles);