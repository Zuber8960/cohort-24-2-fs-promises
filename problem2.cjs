
/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt.
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt.
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt.
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.

*/



//=============================================================================================

const fs = require('fs');
const path = require('path');

const filePath = path.resolve("lipsum.txt");
const fileNames = path.resolve("filenames.txt");
const file1 = path.resolve("file1.txt");
const file2 = path.resolve("file2.txt");
const file3 = path.resolve("file3.txt");


function problem2() {

    function createAndUpdateFile(filePath, content) {
        return new Promise((resolve, reject) => {
            fs.appendFile(filePath, content, (err) => {
                if (err) {
                    reject(err);
                } else {
                    const file = filePath.split("/");
                    const name = file[file.length - 1];
                    console.log(`${name} created or data appended successfully !`);
                    resolve(name);
                }
            })
        })
    };

    function readFileFuntion(filePath) {
        return new Promise((resolve, reject) => {
            fs.readFile(filePath, "utf-8", (err, data) => {
                if (err) {
                    reject(err);
                } else {
                    console.log("file read successfully !");
                    resolve(data);
                }
            })
        })
    };


    function deleteFileFunction(filePath) {
        fs.unlink(filePath, (err) => {
            if (err) {
                console.error(err);
            } else {
                const file = filePath.split("/");
                const name = file[file.length-1];
                console.log(`${name} deleted successfully !`);
            }
        })
    };


    readFileFuntion(filePath)
        .then(result => {
            // console.log(result);
            const readFileData = result.toUpperCase();
            return createAndUpdateFile(file1, readFileData);
        })
        .then(result => {
            // console.log(result);
            const content = result + "\n";
            return createAndUpdateFile(fileNames, content);
        })
        .then(result => {
            // console.log(result);
            return readFileFuntion(file1);
        })
        .then(result => {
            const readFileData = result.toLowerCase().split(".").join("\n");
            return createAndUpdateFile(file2, readFileData);
        })
        .then(result => {
            const content = result + "\n";
            return createAndUpdateFile(fileNames, content);
        })
        .then(result => {
            return readFileFuntion(file2);
        })
        .then(result => {
            const readFileData = result.split("\n").sort().join("\n");
            return createAndUpdateFile(file3, readFileData);
        })
        .then(result => {
            const content = result;
            return createAndUpdateFile(fileNames, content);
        })
        .then(result => {
            return readFileFuntion(fileNames);
        })
        .then(result => {
            // console.log(result);
            const arr = result.split("\n");
            for(let index = 0; index< arr.length; index++){
                const filePath = path.resolve(arr[index]);
                deleteFileFunction(filePath);
            }
        })
        .catch(err => {
            console.log(err);
        })
    };

    // problem2();


    module.exports = problem2;


//=============================================================================================path