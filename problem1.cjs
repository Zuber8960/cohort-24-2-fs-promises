/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/



//====================================================================================


const fs = require('fs');

const path = require('path');
const folderPath = path.resolve("randomFolder");



function problem1(folderPath, randomFilesNumber) {

    new Promise((resolve, reject) => {
        return fs.mkdir(folderPath, function (err) {
            if (err) {
                // console.log(err);
                reject(err);
            } else {
                // console.log('folder created successfully.');
                resolve('folder created successfully.');
            };
        });
    })
        .then(result => {
            console.log(result);
            return new Promise((resolve, reject) => {
                const files = [];
                for (let index = 1; index <= randomFilesNumber; index++) {
                    let filePath = path.resolve(folderPath, `file${index}.json`);
                    fs.writeFile(filePath, "{}", function (err) {
                        if (err) {
                            reject(err);
                        } else {
                            // console.log("Success");
                            files.push(`file${index}.json`);
                            if (files.length === randomFilesNumber) {
                                console.log("All files have been created !");
                                resolve(files);
                            };
                        };
                    });
                };
            });
        })
        .then(result => {
            // console.log(result);
            return deleteFiles(result);
        })
        .catch(err => {
            console.error(err);
        });

    function deleteFiles(data) {
        for (let index = 0; index < data.length; index++) {
            let filePath = path.resolve(folderPath, `${data[index]}`);
            fs.unlink(filePath, function (err) {
                if (err) {
                    console.error(err);
                } else {
                    console.log("Deleted");
                }
            })
        };
    };
}

// problem1("./random", 10);


module.exports = problem1;
